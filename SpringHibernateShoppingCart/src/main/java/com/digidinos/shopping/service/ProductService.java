package com.digidinos.shopping.service;

import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.digidinos.shopping.dao.ProductRepository;
import com.digidinos.shopping.entity.Product;

@Service
public class ProductService {
	@Autowired
	ProductRepository productRepository;

	public List<Product> findAll() {
		return this.productRepository.findAll();
	}

	public void add(Product product) {
		this.productRepository.save(product);

	}

	public void update(Product product) {
		this.productRepository.save(product);

	}

	public void delete(int id) {
		// TODO Auto-generated method stub
		this.productRepository.deleteById(id);
	}

	public Optional<Product> selectbyid(int id) {

		return productRepository.findById(id);

	}
}
