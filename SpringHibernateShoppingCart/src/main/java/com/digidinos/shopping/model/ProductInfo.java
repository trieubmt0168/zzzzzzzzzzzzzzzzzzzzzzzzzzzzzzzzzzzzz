package com.digidinos.shopping.model;

import com.digidinos.shopping.entity.Product;

public class ProductInfo {
	private int id;
    private String code;
    private String name;
    private double price;
    private boolean isDelete;
    public ProductInfo() {
    }
 
    public ProductInfo(Product product) {
    	this.id = product.getId();
    	this.code = product.getCode();
        this.name = product.getName();
        this.price = product.getPrice();
        this.isDelete = product.isDelete();
    }
 
    // Sử dụng trong JPA/Hibernate query
    public ProductInfo(int id,String code, String name, double price,boolean isDelete) {
        this.code = code;
        this.id = id;
        this.name = name;
        this.price = price;
        this.isDelete= isDelete;
    }
 
    public String getCode() {
        return code;
    }
 
    public void setCode(String code) {
        this.code = code;
    }
 
    public String getName() {
        return name;
    }
 
    public void setName(String name) {
        this.name = name;
    }
 
    public double getPrice() {
        return price;
    }
 
    public void setPrice(double price) {
        this.price = price;
    }

	public boolean isDelete() {
		return isDelete;
	}

	public void setDelete(boolean isDelete) {
		this.isDelete = isDelete;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}
 
}