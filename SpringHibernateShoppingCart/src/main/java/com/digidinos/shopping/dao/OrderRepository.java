package com.digidinos.shopping.dao;

import com.digidinos.shopping.entity.Order;

public interface OrderRepository<ID> extends BaseRepository<Order, ID>{
	
}
