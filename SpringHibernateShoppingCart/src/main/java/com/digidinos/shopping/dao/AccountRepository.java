package com.digidinos.shopping.dao;

import java.util.List;
import java.util.Optional;

import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.BaseEntity;

public interface AccountRepository<ID> extends BaseRepository<BaseEntity, ID>{

	@Override
    default List<BaseEntity> findAll() {
        return this.findAll();
    }

    Account findByUserName(String userName);

    public default Optional<BaseEntity> findById(int id) {
        return this.findById(id);
    }

    public default void deleteById(String id) {
        this.deleteById(id);
    }

    public default void delete(BaseEntity baseEntity) {
        this.delete(baseEntity);
    }
}
