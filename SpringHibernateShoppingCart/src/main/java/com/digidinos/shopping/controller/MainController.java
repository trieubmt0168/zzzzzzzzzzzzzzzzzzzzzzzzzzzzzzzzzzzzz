package com.digidinos.shopping.controller;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.dao.AccountRepository;
import com.digidinos.shopping.dao.ProductRepository;
import com.digidinos.shopping.entity.Account;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.CustomerForm;
import com.digidinos.shopping.model.CartInfo;
import com.digidinos.shopping.model.CustomerInfo;
import com.digidinos.shopping.model.ProductInfo;
import com.digidinos.shopping.pagination.PaginationResult;
import com.digidinos.shopping.service.ProductService;
import com.digidinos.shopping.util.Utils;
import com.digidinos.shopping.validator.CustomerFormValidator;

@Controller
@Transactional
public class MainController {

//	@Autowired
//	private OrderDAO orderDAO;
//
//	@Autowired
//	private ProductDAO productDAO;

	@Autowired
	private CustomerFormValidator customerFormValidator;
	@Autowired
	private ProductService productService;
	@Autowired
	private AccountRepository accountRepository;

	@Autowired
	private ProductRepository productRepository;

	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		// Trường hợp update SL trên giỏ hàng.
		// (@ModelAttribute("cartForm") @Validated CartInfo cartForm)
		if (target.getClass() == CartInfo.class) {

		}

		// Trường hợp save thông tin khách hàng.
		// (@ModelAttribute @Validated CustomerInfo customerForm)
		else if (target.getClass() == CustomerForm.class) {
			dataBinder.setValidator(customerFormValidator);
		}

	}

	@RequestMapping("/403")
	public String accessDenied() {
		return "/403";
	}

	@RequestMapping(value = { "/", "home" })
	public String home(Model model //
//			@RequestParam(value = "name", defaultValue = "") String likeName,
//			@RequestParam(value = "page", defaultValue = "1") int page
	) {
//		final int maxResult = 8;
//		final int maxNavigationPage = 10;
		List<Product> product = productService.findAll();
//		PaginationResult<ProductInfo> result = productDAO.queryProducts(page, //
//				maxResult, maxNavigationPage, likeName);

		model.addAttribute("paginationProducts", product);
		return "index";
	}

//	 Danh sách sản phẩm.
	@RequestMapping(value = { "/admin/productList" }, method = RequestMethod.GET)
	public String listProductHandler(Model model, //
			@RequestParam(value = "name", defaultValue = "") String likeName,
			@RequestParam(value = "page", defaultValue = "1") int page) {
//		final int maxResult = 5;
//		final int maxNavigationPage = 10;
//
//		PaginationResult<ProductInfo> result = productDAO.queryProducts(page, //
//				maxResult, maxNavigationPage, likeName);
		List<Product> products = productService.findAll();
		model.addAttribute("paginationProducts", products);
		return "adminProduct";
	}

//	@RequestMapping({ "/buyProduct" })
//	public String listProductHandler(HttpServletRequest request, Model model, //
//			@RequestParam(value = "code", defaultValue = "") String code) {
//
//		Product product = null;
//		if (code != null && code.length() > 0) {
//			product = productDAO.findProduct(code);
//		}
//		if (product != null) {
//
//			//
//			CartInfo cartInfo = Utils.getCartInSession(request);
//
//			ProductInfo productInfo = new ProductInfo(product);
//
//			cartInfo.addProduct(productInfo, 1);
//		}
//
//		return "redirect:/shoppingCart";
//	}

//	@RequestMapping(value = { "/shoppingCartRemoveProduct" }, method = RequestMethod.GET)
//	public String removeProductHandler(HttpServletRequest request, Model model, //
//			@RequestParam(value = "code", defaultValue = "") String code) {
//		Product product = null;
//		if (code != null && code.length() > 0) {
//			product = productDAO.findProduct(code);
//		}
//		if (product != null) {
//
//			CartInfo cartInfo = Utils.getCartInSession(request);
//
//			ProductInfo productInfo = new ProductInfo(product);
//
//			cartInfo.removeProduct(productInfo);
//
//		}
//
//		return "redirect:/shoppingCart";
//	}

	// POST: Cập nhập số lượng cho các sản phẩm đã mua.
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.POST)
	public String shoppingCartUpdateQty(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("cartForm") CartInfo cartForm) {

		CartInfo cartInfo = Utils.getCartInSession(request);
		cartInfo.updateQuantity(cartForm);

		return "redirect:/shoppingCart";
	}

	// GET: Hiển thị giỏ hàng.
	@RequestMapping(value = { "/shoppingCart" }, method = RequestMethod.GET)
	public String shoppingCartHandler(HttpServletRequest request, Model model) {

		CartInfo myCart = Utils.getCartInSession(request);

		model.addAttribute("cartForm", myCart);
		return "shoppingCart";
	}

	// GET: Nhập thông tin khách hàng.
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.GET)
	public String shoppingCartCustomerForm(HttpServletRequest request, Model model) {

		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		}
		CustomerInfo customerInfo = cartInfo.getCustomerInfo();

		CustomerForm customerForm = new CustomerForm(customerInfo);

		model.addAttribute("customerForm", customerForm);

		return "shoppingCartCustomer";
	}

	// POST: Save thông tin khách hàng.
	@RequestMapping(value = { "/shoppingCartCustomer" }, method = RequestMethod.POST)
	public String shoppingCartCustomerSave(HttpServletRequest request, //
			Model model, //
			@ModelAttribute("customerForm") @Validated CustomerForm customerForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			customerForm.setValid(false);
			// Forward tới trang nhập lại.
			return "shoppingCartCustomer";
		}

		customerForm.setValid(true);
		CartInfo cartInfo = Utils.getCartInSession(request);
		CustomerInfo customerInfo = new CustomerInfo(customerForm);
		cartInfo.setCustomerInfo(customerInfo);

		return "redirect:/shoppingCartConfirmation";
	}

	// GET: Xem lại thông tin để xác nhận.
	@RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.GET)
	public String shoppingCartConfirmationReview(HttpServletRequest request, Model model) {
		CartInfo cartInfo = Utils.getCartInSession(request);

		if (cartInfo == null || cartInfo.isEmpty()) {

			return "redirect:/shoppingCart";
		} else if (!cartInfo.isValidCustomer()) {

			return "redirect:/shoppingCartCustomer";
		}
		model.addAttribute("myCart", cartInfo);

		return "shoppingCartConfirmation";
	}

//    // POST: Gửi đơn hàng (Save).
//    @RequestMapping(value = { "/shoppingCartConfirmation" }, method = RequestMethod.POST)
// 
//    public String shoppingCartConfirmationSave(HttpServletRequest request, Model model) {
//        CartInfo cartInfo = Utils.getCartInSession(request);
// 
//        if (cartInfo.isEmpty()) {
// 
//            return "redirect:/shoppingCart";
//        } else if (!cartInfo.isValidCustomer()) {
// 
//            return "redirect:/shoppingCartCustomer";
//        }
//        try {
//            orderDAO.saveOrder(cartInfo);
//        } catch (Exception e) {
// 
//            return "shoppingCartConfirmation";
//        }
// 
//        // Xóa giỏ hàng khỏi session.
//        Utils.removeCartInSession(request);
// 
//        // Lưu thông tin đơn hàng cuối đã xác nhận mua.
//        Utils.storeLastOrderedCartInSession(request, cartInfo);
// 
//        return "redirect:/shoppingCartFinalize";
//    }
//	@RequestMapping(value = { "/shoppingCartFinalize" }, method = RequestMethod.GET)
//	public String shoppingCartFinalize(HttpServletRequest request, Model model) {
//
//		CartInfo lastOrderedCart = Utils.getLastOrderedCartInSession(request);
//
//		if (lastOrderedCart == null) {
//			return "redirect:/shoppingCart";
//		}
//		model.addAttribute("lastOrderedCart", lastOrderedCart);
//		return "shoppingCartFinalize";
//	}
//
//	@RequestMapping(value = { "/productImage" }, method = RequestMethod.GET)
//	public void productImage(Model model, @RequestParam(value = "ID", defaultValue = "") int id) throws IOException {
//		Optional<Product> product = new ArrayList<Product>();
//		if (id != 0) {
//			product = this.productRepository.findById(id);
//		}
//		if (product != null && product.getImage() != null) {
//			response.setContentType("image/jpeg, image/jpg, image/png, image/gif");
//			response.getOutputStream().write(product.getImage());
//		}
//		response.getOutputStream().close();
//	}

}
