package com.digidinos.shopping.controller;

import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.apache.commons.lang.exception.ExceptionUtils;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.ui.ModelMap;
import org.springframework.validation.BindingResult;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.WebDataBinder;
import org.springframework.web.bind.annotation.InitBinder;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.digidinos.shopping.dao.ProductRepository;
import com.digidinos.shopping.entity.Product;
import com.digidinos.shopping.form.ProductForm;
import com.digidinos.shopping.model.OrderDetailInfo;
import com.digidinos.shopping.model.OrderInfo;
import com.digidinos.shopping.pagination.PaginationResult;
import com.digidinos.shopping.service.ProductService;

@Controller
//@Transactional
public class AdminController {

//    @Autowired
//    private OrderDAO orderDAO;

	@Autowired
	private ProductService productService;

// 
//    @Autowired
//    private ProductFormValidator productFormValidator;
// 
	@InitBinder
	public void myInitBinder(WebDataBinder dataBinder) {
		Object target = dataBinder.getTarget();
		if (target == null) {
			return;
		}
		System.out.println("Target=" + target);

		if (target.getClass() == ProductForm.class) {
//            dataBinder.setValidator(productFormValidator); 
		}
	}
// 

	// GET: Hiển thị trang login
	@RequestMapping(value = { "/login" }, method = RequestMethod.GET)
	public String login(Model model) {

		return "login";
	}

	@RequestMapping(value = { "/admin/accountInfo" }, method = RequestMethod.GET)
	public String accountInfo(Model model) {

		UserDetails userDetails = (UserDetails) SecurityContextHolder.getContext().getAuthentication().getPrincipal();
		System.out.println(userDetails.getPassword());
		System.out.println(userDetails.getUsername());
		System.out.println(userDetails.isEnabled());

		model.addAttribute("userDetails", userDetails);
		return "accountInfo";
	}

//    @RequestMapping(value = { "/admin/orderList" }, method = RequestMethod.GET)
//    public String orderList(Model model, //
//            @RequestParam(value = "page", defaultValue = "1") String pageStr) {
//        int page = 1;
//        try {
//            page = Integer.parseInt(pageStr);
//        } catch (Exception e) {
//        }
//        final int MAX_RESULT = 5;
//        final int MAX_NAVIGATION_PAGE = 10;
// 
//        PaginationResult<OrderInfo> paginationResult //
//                = orderDAO.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
// 
//        model.addAttribute("paginationResult", paginationResult);
//        return "orderList";
//    }
//    @RequestMapping(value = { "/admin/Order" }, method = RequestMethod.GET)
//    public String AdminorderList(Model model, //
//            @RequestParam(value = "page", defaultValue = "1") String pageStr) {
//        int page = 1;
//        try {
//            page = Integer.parseInt(pageStr);
//        } catch (Exception e) {
//        }
//        final int MAX_RESULT = 5;
//        final int MAX_NAVIGATION_PAGE = 10;
// 
//        PaginationResult<OrderInfo> paginationResult //
//                = orderDAO.listOrderInfo(page, MAX_RESULT, MAX_NAVIGATION_PAGE);
// 
//        model.addAttribute("paginationResult", paginationResult);
//        return "adminOrder";
//    }
	//////
	@RequestMapping(value = { "/admin" }, method = RequestMethod.GET)
	public String Adminhome(Model model) {

		return "adminHome";
	}

	// GET: Hiển thị product
    @RequestMapping(value = { "/admin/addProduct" }, method = RequestMethod.GET)
    public String product(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
        ProductForm productForm = null;
 
        if (id != null && id > 0) {
            Product product =  (Product) productService.findAll();
            if (product != null) {
                productForm = new ProductForm(product);
            }
        }
        if (productForm == null) {
            productForm = new ProductForm();
            productForm.setNewProduct(true);
        }
        model.addAttribute("productForm", productForm);
        return "addProduct";
    }
//     GET: Hiển thị edit
	@RequestMapping(value = { "/admin/editProduct" }, method = RequestMethod.GET)
	public String product1(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		ProductForm productForm = null;

		if (id != null) {
			Optional<Product> productOpt = productService.selectbyid(id);
			if (productOpt.isPresent()) {
				Product product = productOpt.get();
				productForm = new ProductForm(product);
			}
		}

		if (productForm == null) {
			productForm = new ProductForm();
			productForm.setNewProduct(true);
		}
		model.addAttribute("productForm", productForm);
		return "editProduct";
	}

	// POST: Save product
    @RequestMapping(value = { "/admin/addProduct" }, method = RequestMethod.POST)
    public String productSave(Model model, //
            @ModelAttribute("productForm") @Validated ProductForm productForm, //
            BindingResult result, //
            final RedirectAttributes redirectAttributes) {
 
        if (result.hasErrors()) {
            return "addProduct";
        }
        try {
           
			productService.add(productForm.getProduct());
        } catch (Exception e) {
            Throwable rootCause = ExceptionUtils.getRootCause(e);
            String message = rootCause.getMessage();
            model.addAttribute("errorMessage", message);
            // Show product form.
            return "addProduct";
        }

        return "redirect:/admin/productList";
    }
	@RequestMapping(value = { "/admin/editProduct" }, method = RequestMethod.POST)
	public String productSave1(Model model, //
			@ModelAttribute("productForm") @Validated ProductForm productForm, //
			BindingResult result, //
			final RedirectAttributes redirectAttributes) {

		if (result.hasErrors()) {
			return "editProduct";
		}
		try {
			productService.update(productForm.getProduct());
		} catch (Exception e) {
			Throwable rootCause = ExceptionUtils.getRootCause(e);
			String message = rootCause.getMessage();
			model.addAttribute("errorMessage", message);
			// Show product form.
			return "editProduct";
		}

		return "redirect:/admin/productList";
	}

//    @RequestMapping(value = { "/admin/order" }, method = RequestMethod.GET)
//    public String orderView(Model model, @RequestParam("orderId") String orderId) {
//        OrderInfo orderInfo = null;
//        if (orderId != null) {
//          //  orderInfo = this.orderDAO.getOrderInfo(orderId);
//        }
//        if (orderInfo == null) {
//            return "redirect:/admin/orderList";
//        }
//        List<OrderDetailInfo> details = this.orderDAO.listOrderDetailInfos(orderId);
//        orderInfo.setDetails(details);
// 
//        model.addAttribute("orderInfo", orderInfo);
// ModelMap map,@Validated @PathVariable("id_dm") int id_dm,RedirectAttributes redirect
//        return "order";
//    }
	@RequestMapping(value = { "admin/deleteProduct" }, method = RequestMethod.GET)
	public String removeProductHandler(Model model, @RequestParam(value = "id", defaultValue = "") Integer id) {
		if ( id !=null) {
			productService.delete(id);
		}
		return "redirect:/admin/productList";
	}
}
